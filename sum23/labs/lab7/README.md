# Lab 7: Dictionaries

In this lab, you will analyze water accessibility data using dictionaries.

### Corrections/Clarifications

None yet

**Find any issues?** Let Jane or Adi know during lab, or create a post on Piazza

------------------------------

## Learning Objectives

In this lab, you will practice how to...
* access and utilize data in CSV files,
* deal with messy real-world datasets,
* use dictionaries to organize data into key, value pairs.

------------------------------
## Segment 1: Setup

Create a `lab7` directory and download the following files into the `lab7` directory.

* `water_accessibility.csv`
* `practice.ipynb`
* `practice_test.py`

If you found your `.csv` file is downloaded as a `.txt` file (e.g. `water_accessibility.txt` instead of `water_accessibility.csv`), run `mv water_accessibility.txt water_accessibility.csv` from your Powershell/Terminal to change the extension of the file into `.csv` file manually. All the data that we need for p7 is stored in `water_accessibility.csv`

To start, familiarize yourself with the dataset (`water_accessibility.csv`). Examine its contents using Microsoft Excel, Numbers (Mac) or any other spreadsheet viewing software.

------------------------------

## Segments 2: Using dictionaries to analyze the data

You will be finishing the rest of your lab on `practice.ipynb`. Run the command `jupyter notebook` from your Terminal/PowerShell window.
Remember not to close this
Terminal/PowerShell window while Jupyter is running, and open a new Terminal/PowerShell
window if necessary.

**Note**: For p7, you will be working on `p7.ipynb`, which is very similar to `practice.ipynb`. We
strongly recommend that you finish working on this notebook during the lab, so you can ask
your TA/PM any questions about the notebook that may arise.

**Note**: Unlike `p7.ipynb`, you do **not** have to submit `practice.ipynb`. This notebook is solely
for your practice and preparation for p7.

------------------------------

## Segment 3: Otter tests check for project submission

This segment is informational only and has no tasks. Your work on `p7.ipynb` is not complete when you submit the project on Gradescope. You **must** review the project's rubric and make sure that you have followed the directions provided in the project to solve the questions. The rubric is meant to reinforce your understanding of the project's directions. The hidden tests on the autograder will be making deductions as per the rubric. If you feel that the autograder has falsely identified your code as incorrect, make a regrade request, so that TAs and graders manually review your code to confirm whether the deduction is appropriate.

------------------------------

## Project 7

You can now get started with [p7]((https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/blob/main/sum23/projects/p7)). **You may use any helper functions created here in project p7**. Remember to only work with p7 with your partner from this point on. Have fun!
