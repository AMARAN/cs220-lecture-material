# lab3: Learning an API and some functions

### Corrections/Clarifications

- None yet.

**Find any issues?** Talk to your TA or peer mentor in lab, or create a Piazza post.

------------------------------
## Learning Objectives

In this lab, you will practice...
* Writing functions with return statements
* Importing a module and using its functions
* Using parameters' default values when calling functions
* Avoiding hardcoding by using a `get_id` function
* Working with the index of a row of data

------------------------------
## Note on Academic Misconduct

You may do these lab exercises only with your project partner; you are not allowed to start working on lab3 with one person, then do the project with a different partner.  Now may be a good time to review [our course policies](https://canvas.wisc.edu/courses/355767/pages/syllabus?module_item_id=6048035).

------------------------------

## Project partner

We strongly recommend students find a project partner. Pair programming is a great way to learn from a fellow student. Project difficulty increases exponentially in this course. Finding a project partner early on during the semester is a good idea.

If you are still looking for a project partner, take a moment now to ask around the room if anyone would like to partner with you on this project. Then you can work with them on this lab as well as the project.

------------------------------
## Description

For many projects this semester, we'll provide you with a *module* (a collection of functions) named `project`, in a file named `project.py`. This module will provide functions that will help you complete the project. In the lab, we will introduce the module `project.py` which you will need to use in `p3`.

When using an unfamiliar module, the first thing you should do is study the module's *API*. API stands for "Application Programming Interface".
The API descibes everything a programmer needs to know about a piece of the module in order to use it. Understanding the API will involve learning about each function and the arguments it takes, and what functions might need to be called before you can use other functions.

There are two ways you can learn about an API. First, the person who created the API may have provided written directions, called *documentation*. Second, there are ways you can write code to learn about a collection of functions; this approach is called *inspection*.

------------------------------
## Segment 1: Setup

Create a `lab3` directory and download the following files into the `lab3` directory:

* `lab.csv`
* `project.py`
* `practice.ipynb`
* `practice_test.py`

Once you have downloaded the files, open a terminal and navigate to your `lab3` directory.  Run `ls` to make sure the above files are available.

**Note:** If you accidentally downloaded the file as a `.txt` instead of `.csv` (say `lab.txt`), you can execute `mv lab.txt lab.csv` on a Terminal/PowerShell window. Recall that the `mv` (move) command lets you rename a source file (first argument, example: `lab.txt`) to the destination file (second argument, example: `lab.csv`).

------------------------------

## Segment 2: Learning the `project.py` API

The file `project.py` contains certain *functions* that will be useful for you when you are solving `p3`. It is not necessary to understand *how* these functions work (although you will learn how they work within a few weeks), but to use this module, you need to know *what* these functions do.
When dealing with an unfamiliar module, the best way to learn what its functions are, and how to use them, is to study the module's API. In this segment, we will be learning how to do exactly that.

First, open a new Terminal/PowerShell window, and navigate to the `lab3` folder which contains `project.py`. From here, type `python` (or `python3` if that is what worked for you in [lab2](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/tree/main/sum23/labs/lab2)) to enter the Interactive Mode. It is also recommended that you review [lab2](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/tree/main/sum23/labs/lab2#task-16-exit-interactive-mode) on how to exit Interactive mode.

### Task 2.1: Using `dir`

From the Interactive mode, type the following command:

```python
>>> import project
```
This line *imports* the `project` module, so you can use the functions inside it. If you want to use any of the functions inside any module, you will have to import it first. But before we can use the functions inside this module, we need to find out *what* functions are inside this module. To do that, type the following command in Interactive mode:

```python
>>> dir(project)
```

You should see the following output:

```
['__builtins__', '__cached__', '__csv__', '__doc__', '__file__', '__id_to_data__', '__loader__', '__name__', '__name_to_id__', '__package__', '__spec__', '__years__', 'dump', 'get_id', 'get_sales', 'init']
```

The functions inside this module that will be relevant to us are the ones that do **not** begin and end with two underscores.

### Task 2.2: Inspecting `project.py`

Now that we know the functions inside the module that we can call, we need to figure out *what* these functions do. One way to do that is to just try and call them. Try the following on Interactive mode:

```python
>>> project.dump()
```

You will likely see the following error message:

```
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "C:\Users\myname\Documents\cs220\lab3", line 49, in dump
    raise Exception("you did not call init first")
Exception: you did not call init first
```

This tells us that before we can call `project.dump`, we will need to call the `project.init` function.

### Task 2.3: Using `help`

We can continue to try and inspect the other functions in the `project` module to figure out what they do. However, most modules come with *documentation* explaining what each of the functions does, and it can save us time if we read this documentation. Try the following on Interactive mode:

```python
>>> help(project.init)
```

You should see the following output:

```
Help on function init in module project:

init(path)
    init(path) must be called to load data before other calls will work.  You should call it like this: init("car_sales_data.csv") or init("lab.csv")
```

**Note:** If you are using Mac OS, you **may** enter **Python help mode** when you type `help(project.init)`. You can recognize that you are in help mode if you no longer see the prompt `>>>` appearing on your screen. You will not be able to execute other Python commands from this mode. In order to *exit* the help mode, you need to type `q` and hit the `RETURN` key. If you do not enter Python help mode, this is unnecessary.

The documentation here tells us that the function `init` takes in a `path` such as `lab.csv`, or `car_sales_data.csv` (which you will work with in p3) as its argument, and loads the data from the file into the Python program. Can you now understand what the Traceback was telling us when we called `project.dump` earlier?

Let us now load the data from `lab.csv`. Execute the following command on Interactive mode.

```python
>>> project.init('lab.csv')
```

**Note:** If you load the file `lab.csv` correctly, you will see the following warning message:
```
WARNING!  Opening a path other than car_sales_data.csv.  That's fine for testing your code yourself, but car_sales_data.csv will be the only file around when we test your code for grading.
```
That is to be expected. It is warning you that for the project `p3`, you will not be working with the data in `lab.csv`, but instead, the data in `car_sales_data.csv`, and that you should be careful not to load in the wrong file when working on `p3`.

Now that we have loaded in our data, let us now see what `project.dump` does. Execute the following command on Interactive mode.

```python
>>> help(project.dump)
```

You should see the following output:

```
Help on function dump in module project:

dump()
    prints all the data to the screen
```

Can you figure out what this function does, and how to call it? Call the function yourself. You should see the following output:

```
Chevy Bolt [ID: 21]
  2019: 16313 cars sold
  2020: 20754 cars sold
  2021: 24828 cars sold

Chrysler Pacifica Plug-in Hybrid [ID: 3664]
  2019: 5811 cars sold
  2020: 9165 cars sold
  2021: 28747 cars sold

Honda Clarity Plug-in [ID: 426]
  2019: 10728 cars sold
  2020: 4157 cars sold
  2021: 2366 cars sold

Porsche Cayenne S E-Hybrid [ID: 6273]
  2019: 1140 cars sold
  2020: 1758 cars sold
  2021: 1783 cars sold

Tesla Model 3 [ID: 850]
  2019: 154840 cars sold
  2020: 122700 cars sold
  2021: 121877 cars sold
```

This is data on the number of a few select models of electric cars sold between 2019 and 2021. If you manually open `lab.csv` using Microsoft Excel or some other Spreadsheet software, you will find this data stored there.

We now need to figure out how to use the other functions in the module. Read the *documentation* using `help` to figure out what the following functions do:

- `project.get_id`
- `project.get_sales`

------------------------------

## Segment 3: Solving `practice.ipynb`

You will be finishing the rest of your lab on `practice.ipynb`. Exit Python Interactive mode on your Terminal/PowerShell (using the `exit` function, as in [lab2](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/tree/main/sum23/labs/lab2#task-16-exit-interactive-mode)), and run the command `jupyter notebook`. Remember not to close this terminal window while Jupyter is running, and open a new Terminal window if necessary.

**Note:** For `p3`, you will be working on `p3.ipynb` which is very similar to `practice.ipynb`. It is strongly recommended that you finish working on this notebook during the lab, so you can ask your TA/PM any questions about the notebook that may arise.

**Note:** Unlike `p3.ipynb`, you do **not** have to submit `practice.ipynb`. This notebook is solely for your practice.

------------------------------
## Project 3

Great, now you're ready to start [P3](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/tree/main/sum23/projects/p3)! Remember to only work with your partner from this lab on p3 from this point on. Have fun!
